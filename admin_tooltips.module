<?php

/**
 * @file
 * Main module functions for the admin tooltips module.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\WidgetInterface;
use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Implements hook_field_widget_third_party_settings_form().
 */
function admin_tooltips_field_widget_third_party_settings_form(WidgetInterface $plugin, FieldDefinitionInterface $field_definition, $form_mode, $form, FormStateInterface $form_state): array {
  $element = [];
  $plugin_third_party_settings = $plugin->getThirdPartySettings('admin_tooltips');

  $element['admin_tooltip'] = [
    '#type' => 'details',
    '#title' => t("Tooltip settings"),
    '#weight' => -4,
    '#open' => TRUE,
  ];

  $element['admin_tooltip']['admin_tooltip_text'] = [
    '#type' => 'textarea',
    '#title' => t('Text'),
    '#description' => t("Text to be used in tooltip of this field"),
    '#default_value' => $plugin_third_party_settings['admin_tooltip']['admin_tooltip_text'] ?? '',
  ];

  return $element;
}

/**
 * Implements hook_field_widget_form_alter().
 */
function admin_tooltips_field_widget_form_alter(&$element, FormStateInterface $form_state, $context): void {
  admin_tooltips_field_widget_single_element_form_alter($element, $form_state, $context);
}

/**
 * Implements hook_field_widget_single_element_form_alter().
 */
function admin_tooltips_field_widget_single_element_form_alter(array &$element, \Drupal\Core\Form\FormStateInterface $form_state, array $context): void {
  $third_party_settings = $context['widget']->getThirdPartySettings('admin_tooltips', 'admin_tooltip');
  if (empty($third_party_settings)) {
    return;
  }
  $tooltip_text = $third_party_settings['admin_tooltip']['admin_tooltip_text'];
  $element['#attached']['library'][] = 'admin_tooltips/tooltips';
  if (isset($element['value'])) {
    if ($element['value']['#type'] === 'datetime' || $element['value']['#type'] === 'datelist') {
      $element['#admin_tooltip'] = $tooltip_text;
    }
    else {
      $element['value']['#admin_tooltip'] = $tooltip_text;
    }
  }
  elseif (isset($element['target_id'])) {
    $element['target_id']['#admin_tooltip'] = $tooltip_text;
  }
  elseif (in_array($context['widget']->getPluginId(), ['link_default', 'linkit'])) {
    $element['uri']['#admin_tooltip'] = $tooltip_text;
  }
  else {
    $element['#admin_tooltip'] = $tooltip_text;
  }
}

/**
 * Implements hook_field_widget_settings_summary_alter().
 */
function admin_tooltips_field_widget_settings_summary_alter(&$summary, $context): void {
  $third_party_settings = $context['widget']->getThirdPartySettings('admin_tooltips', 'admin_tooltip');

  if (!empty($third_party_settings)) {
    if (!empty($third_party_settings['admin_tooltip']['admin_tooltip_text'])) {
      $text = $third_party_settings['admin_tooltip']['admin_tooltip_text'];
      $text = strlen($text) > 30 ? substr($text, 0, 30) . "..." : $text;
      $summary[] = t('Tooltip: @tooltip', ['@tooltip' => $text]);
    }
  }
}

/**
 * Implements hook_preprocess_form_element().
 */
function admin_tooltips_preprocess_form_element(&$variables): void {
  $element =& $variables['element'];
  $variables['admin_tooltip'] = $element['#admin_tooltip'] ?? NULL;
}

/**
 * Implements hook_theme().
 */
function admin_tooltips_theme($existing, $type, $theme, $path): array {
  return [
    'form_element__admin_tooltips' => [
      'base hook' => 'form_element',
    ],
  ];
}

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 *
 * This hook adds a new theme suggestion for all form elements.
 */
function admin_tooltips_theme_suggestions_form_element_alter(array &$suggestions, array $variables): void {
  if (!empty($variables['element']['#admin_tooltip'])) {
    $suggestions[] = 'form_element__admin_tooltips';
  }
}


/**
 * Implements hook_theme_suggestions_HOOK_alter().
 *
 * This hook adds a new theme suggestion for containers, like paragraph fields.
 */
function admin_tooltips_theme_suggestions_container_alter(array &$suggestions, array $variables): void {
  admin_tooltips_theme_suggestions_form_element_alter($suggestions, $variables);
}
